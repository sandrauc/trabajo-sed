----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.11.2020 23:02:52
-- Design Name: 
-- Module Name: prueba_estados1_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity prueba_estados1_tb is
--  Port ( );
end prueba_estados1_tb;

architecture Behavioral of prueba_estados1_tb is

    --Inputs
    signal RESET, CLK, piso1, piso2, piso3, piso4: std_logic;
    
    --Outputs
    signal light: std_logic_vector(6 downto 0);
    
    component ascensor is
        PORT (
            RESET: in std_logic;--Reset as�ncrono
            CLK : in std_logic;--Reloj
            piso1: in std_logic; --boton que nos indica que queremos ir a la planta 1
            piso2: in std_logic;--boton planta 2
            piso3: in std_logic;--boton planta 3
            piso4: in std_logic;--boton planta 4
            light: out std_logic_vector(6 downto 0) --visualizamos la planta del ascensor con uno de los displays de 7 segmentos
        );
    end component;

constant CLK_PERIOD : time := 1 sec / 100_000_000; -- Clock period

begin

    uut: ascensor 
        PORT MAP (
            RESET => RESET,
            CLK => CLK,
            piso1 => piso1,
            piso2 => piso2,
            piso3 => piso3,
            piso4 => piso4,
            light => light
        );

clkgen: process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PERIOD;
        CLK <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

RESET <= '0' after 0.25*CLK_PERIOD,
         '1' after 0.75*CLK_PERIOD;

tester: process
    begin
    	
        wait until RESET = '1';
        wait for 0.25 * CLK_PERIOD;
        piso1 <= '1';
        wait for 0.75 * CLK_PERIOD;
        piso1 <= '0';
        piso3 <= '1';
        wait for 0.75 * CLK_PERIOD;
        piso3 <= '0';
        piso2 <= '1';
        wait for 0.75 * CLK_PERIOD;
        piso2 <= '0';
        piso3 <= '1';
        wait for 0.75 * CLK_PERIOD;
        piso3 <= '0';
        piso4 <= '1';
        wait for 0.75 * CLK_PERIOD;
        
       
        wait for 0.25 * CLK_PERIOD;
        assert false
        	report "[SUCCESS]: simulation finished."
            severity failure;        
    end process;
end Behavioral;
