----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.12.2020 10:12:16
-- Design Name: 
-- Module Name: CLK_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CLK_tb is
--  Port ( );
end CLK_tb;

architecture Behavioral of CLK_tb is

signal entrada, reset, salida: std_logic;

component CLK_400Hz is
Port (
        entrada: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
    );
end component;

constant CLK_PERIOD : time := 1 sec / 100_000_000; -- Clock period

begin
uut: CLK_400Hz PORT MAP (
        entrada => entrada,
        reset   => reset,
        salida  => salida
    );

clkgen: process
    begin
    	entrada <= '0';
        wait for 0.5 * CLK_PERIOD;
        entrada <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;
    
    tester: process
    begin 
    reset <= '1';
    for i in 1 to 1249990 loop
        	wait until entrada = '1';
        end loop;
        wait for 0.25 * CLK_PERIOD;
    assert false
        	report "[SUCCESS]: simulation finished."
            severity failure;
    end process;

end Behavioral;
