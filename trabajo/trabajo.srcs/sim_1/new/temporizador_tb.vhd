----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.11.2020 16:50:19
-- Design Name: 
-- Module Name: temporizador_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity temporizador_tb is
--  Port ( );
end temporizador_tb;

architecture Behavioral of temporizador_tb is
 --Inputs
    signal RESET, CLK, piso1, piso2, piso3, piso4: std_logic;
    
    --Outputs
    signal light: std_logic_vector(6 downto 0);
    
    component ascensor is
        PORT (
            RESET: in std_logic;--Reset as�ncrono
            CLK : in std_logic;--Reloj
            piso1: in std_logic; --boton que nos indica que queremos ir a la planta 1
            piso2: in std_logic;--boton planta 2
            piso3: in std_logic;--boton planta 3
            piso4: in std_logic;--boton planta 4
            light: out std_logic_vector(6 downto 0) --visualizamos la planta del ascensor con uno de los displays de 7 segmentos
        );
    end component;

constant CLK_PERIOD : time := 1 sec / 100_000_000; -- Clock period

begin

    uut: ascensor 
        PORT MAP (
            RESET => RESET,
            CLK => CLK,
            piso1 => piso1,
            piso2 => piso2,
            piso3 => piso3,
            piso4 => piso4,
            light => light
        );

clkgen: process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PERIOD;
        CLK <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

RESET <= '0' after 0.25*CLK_PERIOD,
         '1' after 0.75*CLK_PERIOD;

tester: process
    begin
        wait until RESET = '1';
        wait until CLK = '1';
        wait for 0.25 * CLK_PERIOD;
    	--for i in 1 to 55 loop
        	--wait until CLK = '1';
        --end loop;
        piso2 <= '1';
        
        for i in 1 to 5 loop
        	wait until CLK = '1';
        end loop;
        wait for 0.25 * CLK_PERIOD;
        piso2 <= '0';
        piso1 <= '1';
        
        for i in 1 to 5 loop
        	wait until CLK = '1';
        end loop;
        
       
        wait for 0.25 * CLK_PERIOD;
        assert false
        	report "[SUCCESS]: simulation finished."
            severity failure;        
    end process;



end Behavioral;
