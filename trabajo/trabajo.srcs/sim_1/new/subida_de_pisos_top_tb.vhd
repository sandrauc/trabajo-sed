----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.12.2020 10:08:54
-- Design Name: 
-- Module Name: subida_de_pisos_top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity subida_de_pisos_top_tb is
--  Port ( );
end subida_de_pisos_top_tb;

architecture Behavioral of subida_de_pisos_top_tb is

    signal RESET, CLK, piso1, piso2, piso3, piso4, error: std_logic;
    --signal digsel:  std_logic_vector(6 DOWNTO 0);
    signal digctrl:std_logic_vector(7 DOWNTO 0);
    signal light: std_logic_vector(6 downto 0);

    component top is
    PORT (
        RESET: in std_logic;--Reset as�ncrono
        CLK : in std_logic;--Reloj
        piso1: in std_logic; --boton que nos indica que queremos ir a la planta 1
        piso2: in std_logic;--boton planta 2
        piso3: in std_logic;--boton planta 3
        piso4: in std_logic;--boton planta 4
       -- digsel : IN std_logic_vector(6 DOWNTO 0);
        digctrl : OUT std_logic_vector(7 DOWNTO 0);
        --subo: out std_logic;
        --bajo: out std_logic;
        error : out std_logic;
        --sel1: out std_logic;--Las usamos para probar cosas en simulacion
        --sel2: out std_logic;--Las usamos para probar cosas en simulacion
        light: out std_logic_vector(6 downto 0) --visualizamos la planta del ascensor con uno de los displays de 7 segmentos        
    );
    end component;
    
constant CLK_PERIOD : time := 1 sec / 100_000_000; -- Clock period

begin

    uut: top 
        PORT MAP (
            RESET => RESET,
            CLK => CLK,
            piso1 => piso1,
            piso2 => piso2,
            piso3 => piso3,
            piso4 => piso4,
            --digsel => digsel,
            digctrl => digctrl,
            --subo => subo,
            --bajo => bajo, 
            error => error,
            --sel1 => sel1,
            --sel2 => sel2,
            light => light
        );

clkgen: process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PERIOD;
        CLK <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

--RESET <= '0' after 0.25*CLK_PERIOD,
--         '1' after 0.75*CLK_PERIOD;

tester: process
    begin
    	
        --wait until RESET = '1';
         piso1 <= '0';
          piso2 <= '0';
           piso3 <= '0';
            piso4 <= '0';
        for i in 1 to 20 loop
        	wait until CLK = '1';
        end loop;
        wait for 0.25 * CLK_PERIOD;
        wait until CLK = '1';
        wait for 0.25 * CLK_PERIOD;
        piso3 <= '1';
        wait until CLK = '1';
        wait for 0.25 * CLK_PERIOD;
        for i in 1 to 2500000 loop
        	wait until CLK = '1';
        end loop;
        piso3 <= '0';
        for i in 1 to 150000 loop
        	wait until CLK = '1';
        end loop;
        wait for 0.25 * CLK_PERIOD;
--        piso4 <= '1';
--        wait until CLK = '1';
--        wait for 0.25 * CLK_PERIOD;
--        piso3 <= '0';
--        for i in 1 to 5 loop
--        	wait until CLK = '1';
--        end loop;
--        piso4 <= '0';
--        for i in 1 to 15 loop
--        	wait until CLK = '1';
--        end loop;
--        wait for 0.25 * CLK_PERIOD;
        
--        piso1 <= '1';
--        wait until CLK = '1';
--        wait for 0.25 * CLK_PERIOD;
--        piso1 <= '0';
--        for i in 1 to 15 loop
--        	wait until CLK = '1';
--        end loop;
--        wait for 0.25 * CLK_PERIOD;
        
--        piso2 <= '1';
--        wait until CLK = '1';
--        wait for 0.25 * CLK_PERIOD;
--        piso2 <= '0';
--        for i in 1 to 15 loop
--        	wait until CLK = '1';
--        end loop;
--        wait for 0.25 * CLK_PERIOD;
       
       
       
        wait for 0.25 * CLK_PERIOD;
        assert false
        	report "[SUCCESS]: simulation finished."
            severity failure;        
    end process;


end Behavioral;
