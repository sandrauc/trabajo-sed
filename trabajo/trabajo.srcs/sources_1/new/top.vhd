----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.11.2020 15:37:57
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
 PORT (
        RESET: in std_logic;--Reset as�ncrono
        CLK : in std_logic;--Reloj
        piso1: in std_logic; --boton que nos indica que queremos ir a la planta 1
        piso2: in std_logic;--boton planta 2
        piso3: in std_logic;--boton planta 3
        piso4: in std_logic;--boton planta 4
        RGB: out std_logic_vector(2 downto 0); --Salida al led RGB
        error : out std_logic; --Salida que indica que hemos llegado a un estado de error (lo visualizamos con un led)
        digctrl : OUT std_logic_vector(7 DOWNTO 0); --Vector de salida para la seleccion de los displays de 7 segmentos
        light: out std_logic_vector(6 downto 0) --visualizamos la planta del ascensor con uno de los displays de 7 segmentos        
    );
end top;

architecture Behavioral of top is

signal CLK_400: std_logic; --Reloj reducido a 400Hz
signal button_sync1: std_logic; --Se�al boton planta1 s�ncrona
signal button_edge1: std_logic; --Se�al flanco de subida boton planta1
signal button_sync2: std_logic; --Se�al boton planta2 s�ncrona
signal button_edge2: std_logic; --Se�al flanco de subida boton planta2
signal button_sync3: std_logic; --Se�al boton planta3 s�ncrona
signal button_edge3: std_logic; --Se�al flanco de subida boton planta3
signal button_sync4: std_logic; --Se�al boton planta4 s�ncrona
signal button_edge4: std_logic; --Se�al flanco de subida boton planta4
signal display_sel: std_logic := '0';
signal next_display_sel: std_logic;
signal subo: std_logic; --Se�al interna para indicar movimiento de subir
signal bajo: std_logic; --Se�al interna para indicar movimiento de bajar

component CLK_400Hz 
    Port (
        entrada: in  STD_LOGIC;--Reloj de entrada
        reset  : in  STD_LOGIC; --Reset as�ncrono
        salida : out STD_LOGIC --Reloj de 400HZ de salida
    );
end component;

component ascensor 
    PORT (
        RESET: in std_logic; --Reset as�ncrono
        CLK :  in std_logic; --Reloj
        piso1: in std_logic; --boton que nos indica que queremos ir a la planta 1
        piso2: in std_logic; --boton planta 2
        piso3: in std_logic; --boton planta 3
        piso4: in std_logic; --boton planta 4
        subo: out std_logic; --Salida indicando movimiento ascendente ascensor (necesaria para el led RGB)
        bajo: out std_logic; --Salida indicando movimiento descendente ascensor (necesaria para el led RGB)
        error: out std_logic; --Salida que indica que hemos tenido algun fallo
        light: out std_logic_vector(6 downto 0) --visualizamos la planta en la que se encuentra el ascensor con uno de los displays de 7 segmentos
    );
end component;

component EDGEDTCTR is
    port (
        CLK : in std_logic; --Reloj
        SYNC_IN : in std_logic; --Entrada s�ncrona
        EDGE : out std_logic --Salida flanco de subida
    );
end component;

component SYNCHRNZR is
    port (
        CLK : in std_logic; --Reloj
        ASYNC_IN : in std_logic; --Entrada as�ncrona
        SYNC_OUT : out std_logic --Salida s�ncrona
    );
end component;

component RGB_led is
    PORT (
        CLK: in std_logic; --Reloj
        subir : in std_logic; --Entrada que indiga movimiento de subir
        bajar : in std_logic; --Entrada que indica movimiento de bajar
        RGB : out std_logic_vector(2 downto 0) --Salida al led RGB
    );
end component;

begin

inst_CLK: CLK_400Hz
PORT MAP (
        entrada => CLK,
        reset  => RESET,
        salida => CLK_400
    );

inst_ascensor: ascensor
    PORT MAP (
        RESET => RESET,
        CLK => CLK_400,
        piso1 => button_edge1,
        piso2 => button_edge2,
        piso3 => button_edge3,
        piso4 => button_edge4,
        subo => subo,
        bajo => bajo,
        error => error,
        light => light
    );

process (RESET, CLK_400)
    begin
    if RESET = '0' then
        --No hacemos nada ya que lo hacen los component internamente el tratamiendo del reset
    elsif CLK_400'event and CLK_400 = '1' then
        display_sel <= next_display_sel;
    end if;
end process;

process (display_sel)
    begin
    next_display_sel <= not(display_sel);
    case display_sel is
    when '0' =>
        digctrl <= "01111111";
    when '1' =>
        digctrl <= "10111111";
    when others =>
        digctrl <= "11111111";
    end case;

end process;


-- Control flancos boton 1
inst_SYNCHRNZR1: SYNCHRNZR port map (
    CLK => CLK,
    ASYNC_IN => piso1,
    SYNC_OUT => button_sync1
);

inst_EDGEDTCTR1: EDGEDTCTR port map (
    CLK => CLK,
    SYNC_IN => button_sync1,
    EDGE => button_edge1
);

-- Control flancos boton 2
inst_SYNCHRNZR2: SYNCHRNZR 
    port map (
        CLK => CLK,
        ASYNC_IN => piso2,
        SYNC_OUT => button_sync2
    );

inst_EDGEDTCTR2: EDGEDTCTR port map (
    CLK => CLK,
    SYNC_IN => button_sync2,
    EDGE => button_edge2
);

-- Control flancos boton 3
inst_SYNCHRNZR3: SYNCHRNZR 
    port map (
        CLK => CLK,
        ASYNC_IN => piso3,
        SYNC_OUT => button_sync3
    );

inst_EDGEDTCTR3: EDGEDTCTR port map (
    CLK => CLK,
    SYNC_IN => button_sync3,
    EDGE => button_edge3
);

-- Control flancos boton 4
inst_SYNCHRNZR4: SYNCHRNZR 
    port map (
        CLK => CLK,
        ASYNC_IN => piso4,
        SYNC_OUT => button_sync4
    );

inst_EDGEDTCTR4: EDGEDTCTR port map (
    CLK => CLK,
    SYNC_IN => button_sync4,
    EDGE => button_edge4
);

inst_RGB: RGB_led 
    PORT MAP (
        CLK => CLK_400,
        subir => subo,
        bajar => bajo,
        RGB => RGB
    );

end Behavioral;
