----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.12.2020 11:41:44
-- Design Name: 
-- Module Name: RGB_led - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RGB_led is
    PORT (
        CLK: in std_logic; --Reloj
        subir : in std_logic; --Entrada que indica movimiento de subir
        bajar : in std_logic; --Entrada que indica movimiento de bajar
        RGB : out std_logic_vector(2 downto 0) --Salida al led RGB
    );
end RGB_led;

architecture Behavioral of RGB_led is
    signal RGB_i: std_logic_vector(2 downto 0);
begin
    process (CLK, subir, bajar)
    begin
        if CLK'event and CLK = '1' then
            if subir = '1' then
                RGB_i <= "001"; --Si esta subiendo ponemos el RGB de color rojo
            elsif bajar = '1' then
                RGB_i <= "001"; --Si est� bajando ponemos el RGB de color rojo
            else
                RGB_i <= "010"; --Si no est� en movimiento ponemos el RGB de color verde
            end if;
        end if;
    end process;
    RGB <= RGB_i;
    
end Behavioral;
