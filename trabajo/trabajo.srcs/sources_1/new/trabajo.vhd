----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.11.2020 22:35:02
-- Design Name: 
-- Module Name: ascensor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ascensor is
    PORT (
        RESET: in std_logic; --Reset as�ncrono
        CLK :  in std_logic; --Reloj
        piso1: in std_logic; --boton que nos indica que queremos ir a la planta 1
        piso2: in std_logic; --boton planta 2
        piso3: in std_logic; --boton planta 3
        piso4: in std_logic; --boton planta 4
        subo: out std_logic; --Salida indicando movimiento ascendente ascensor (necesaria para el led RGB)
        bajo: out std_logic; --Salida indicando movimiento descendente ascensor (necesaria para el led RGB)
        error: out std_logic; --Salida que indica que hemos tenido algun fallo
        light: out std_logic_vector(6 downto 0) --visualizamos la planta en la que se encuentra el ascensor con uno de los displays de 7 segmentos
    );
end ascensor;

architecture Behavioral of ascensor is

type STATES is (S1, S2, S3, S4); --S1: planta 1, S2: planta 2, S3: planta 3, S4: planta 4
signal current_state: STATES; --Estado actual
signal next_state: STATES; --Estado siguiente
signal final_state: STATES; --Estado final (para cuando queremos ir a una planta que no es la inmediata siguiente)
signal subir: std_logic; --Se�al que indica que el ascensor esta en movimiento ascendente
signal bajar: std_logic; --Se�al que indica que el ascesor esta en movimiento descendente
signal ciclos: natural; --Temporizador, numero de ciclos de reloj que nos faltan para alcanzar la siguiente planta
signal next_ciclos: natural; --Siguiente numero de ciclos restantes
signal num_ciclos: natural := 1500; --Generico del numero de ciclos de reloj que tarda en pasar de una planta a otra
signal display_sel: std_logic := '0';--unsigned(1 downto 1) := (others => '0');
signal next_display_sel: std_logic;--unsigned(1 downto 1) := (others => '0');


begin


state_register: process (RESET, CLK)
    begin
    if RESET = '0' then
        current_state <= S1; --Por seguridad el estado elegido por el reset es la primera planta
        ciclos <= num_ciclos;
    elsif CLK'event and CLK = '1' then
        display_sel <= next_display_sel;
        if current_state /= next_state then --Hemos cambiado de estado, no se actualiza hasta que no pasa el tiempo necesario para cambiar de planta
            if ciclos > 0 then
                ciclos <= next_ciclos - 1;
            else 
                current_state <= next_state;
                ciclos <= num_ciclos; --Reestablecemos el numero de ciclos para cambiar de planta
            end if;
        else 
            current_state <= next_state;
            ciclos <= num_ciclos; --Inicializamos el numero de ciclos
        end if;  
    end if;
end process;

nextstate_decod: process (RESET, current_state, subir, bajar, final_state, ciclos, piso1, piso2, piso3, piso4)
    begin
    next_ciclos <= ciclos;
    if RESET = '0' then --En el reset como hemos puesto antes se vuelve por seguridad a la planta 1 y se paralizan los movimientos
        subir <= '0';
        bajar <= '0';
        final_state <= S1;
        next_state <= S1;
    else 
        case current_state is
        when S1 =>
            if subir = '1' then
                if final_state = S1 then
                    subir <= '0'; --Desactivamos el movimiento
                elsif final_state /= S1 then
                    subir <= '1';
                else
                    next_state <= S2; --No haria falta pero se incluye por si falla algo
                end if;
            elsif bajar = '1' then --Tiene prioridad estar en mocimiento sobre que alguien pulse un boton
                if final_state = S1 then
                    bajar <= '0'; --Desactivamos el movimiento
                else
                    bajar <= '1';
                    next_state <= S1; --No haria falta pero se incluye por si falla algo
                end if;
            elsif piso1 = '1' then
                next_state <= S1; --Nos mantenemos en la misma planta
            elsif piso2 = '1' then
                final_state <= S2;
                subir <= '1';
                next_state <= S2;
            elsif piso3 = '1' then
                final_state <= S3;
                subir <= '1';
                next_state <= S2; --El siguiente estado seria la planta 2, para llegar a la 3 hay que pasar por la 2
            elsif piso4 = '1' then 
                final_state <= S4;
                subir <= '1';
                next_state <= S2;   
            else 
                next_state <= S1; --Si no se pulsa ningun bot�n nos mantenemos en la misma planta
            end if;
        when S2 =>
            if subir = '1' then
                if final_state = S2 then
                    subir <= '0';
                else
                    subir <= '1';
                    next_state <= S3;
                end if;
            elsif bajar = '1' then
                if final_state = S2 then
                    bajar <= '0';
                else
                    bajar <= '1';
                    next_state <= S1;
                end if;
            elsif piso1 = '1' then
                final_state <= S1;
                bajar <= '1';
                next_state <= S1;
            elsif piso2 = '1' then
                next_state <= S2;
            elsif piso3 = '1' then
                final_state <= S3;
                subir <= '1';
                next_state <= S3; 
            elsif piso4 = '1' then
                final_state <= S4;
                subir <= '1';
                next_state <= S3;   
            else 
                next_state <= S2;
            end if;
        when S3 =>
             if subir = '1' then
                if final_state = S3 then
                    subir <= '0';
                else
                    subir <= '1';
                    next_state <= S4;
                end if;
            elsif bajar = '1' then
                if final_state = S3 then
                    bajar <= '0';
                else
                    bajar <= '1';
                    next_state <= S2;
                end if;
            elsif piso1 = '1' then
                final_state <= S1;
                bajar <= '1';
                next_state <= S2;
            elsif piso2 = '1' then
                final_state <= S2;
                bajar <= '1';
                next_state <= S2;
            elsif piso3 = '1' then
                next_state <= S3; 
            elsif piso4 = '1' then
                final_state <= S4;
                subir <= '1';
                next_state <= S4;   
            else
                next_state <= S3;
            end if;
        when S4 =>
             if subir = '1' then
                if final_state = S4 then
                    subir <= '0';
                else --no deberia hacer falta pero lo a�adimos por si falla algo
                    subir <= '0';
                    next_state <= S4; 
                end if;
            elsif bajar = '1' then 
                if final_state = S4 then
                    bajar <= '0'; --Desactivamos el movimiento
                else
                    bajar <= '1';
                    next_state <= S3; 
                end if;
            elsif piso1 = '1' then
                final_state <= S1;
                bajar <= '1';
                next_state <= S3;
            elsif piso2 = '1' then
                final_state <= S2;
                bajar <= '1';
                next_state <= S3;
            elsif piso3 = '1' then
                final_state <= S3;
                bajar <= '1';
                next_state <= S3; 
            elsif piso4 = '1' then
                next_state <= S4;   
            else 
                next_state <= S4;
            end if;
        when others =>
            error <= '1'; --En principio no puede estar en ningun otro estado, luego nunca deber�a llegar a activarse error
        end case;
     end if;
end process;

output_decod: process (current_state, subir, bajar, display_sel)
    begin
    
    next_display_sel <= not(display_sel); --Variamos la seleccion del display
    light <= (OTHERS => '0');
    
    case display_sel is
    when '0' => --Cuando vale 0, seleccionaremos el display de m�s a la izquierda que nos indica la planta
        case current_state is
        when S1 =>
            LIGHT <= "1001111"; --1 en display
        when S2 =>
            LIGHT <= "0010010"; --2 en display
        when S3 =>
            LIGHT <= "0000110"; --3 en display
        when S4 =>
            LIGHT <= "1001100"; --4 en display
        when others =>
            LIGHT <= (OTHERS => '0');
        end case;
    when '1' => --Cuando vale 1, seleccionaremos el segundo display que nos indica el tipo de movimiento o parada
        if subir = '1' then
            LIGHT <= "0011101";
        elsif bajar = '1' then
            LIGHT <= "1100011";
        else
            LIGHT <= "1111110";
        end if;   
    when others =>
        LIGHT <= (others => '0');
    end case;
    
    if subir = '1' then
        subo <= '1';
    elsif bajar = '1' then
        bajo <= '1';
    else
        subo <= '0';
        bajo <= '0';
     end if;
end process;

end Behavioral;
