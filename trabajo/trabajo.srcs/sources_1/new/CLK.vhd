----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.12.2020 09:55:14
-- Design Name: 
-- Module Name: CLK - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CLK_400Hz is
Port (
        entrada: in  STD_LOGIC; --Reloj de entrada
        reset  : in  STD_LOGIC; --Reset as�ncrono
        salida : out STD_LOGIC --Reloj de 400HZ de salida
    );
end CLK_400Hz;

architecture Behavioral of CLK_400Hz is

signal temporal: STD_LOGIC := '0';
signal contador: integer range 0 to 124999 := 0; --Contador para hacer la reduccion de ciclos (equivale a medio ciclo de reloj, ya que tiene que estar medio arriba y medio abajo)

begin
    divisor_frecuencia: process (reset, entrada) begin
        if (reset = '0') then
            temporal <= '0';
            contador <= 0;
        elsif rising_edge(entrada) then
            if (contador = 124999) then 
                temporal <= NOT(temporal); --Cuando llega a la cuenta se cambia el valor de la salida (medio ciclo '1', medio ciclo '0')
                contador <= 0; 
            else
                contador <= contador + 1;
            end if;
        end if;
    end process;
     
    salida <= temporal;


end Behavioral;
